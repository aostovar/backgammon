/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Network;

/**
 *
 * @author ALI
 */

import GUI.NetworkMainFrame;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class NetworkManager
{
        int portnum = 2244;
        Socket mysocket = null;
        PrintWriter output = null;
        BufferedReader input = null;
        ServerSocket serversoc = null;
        boolean isServer = true;
        byte []serveraddress =new byte[4];
        NetworkMainFrame nmfparent = null;
        

    public NetworkManager(boolean isServer, byte []serveraddress) {
        this.isServer = isServer;
        this.serveraddress = serveraddress;
    }

    public void setNmf(NetworkMainFrame nmfparent) {
        this.nmfparent = nmfparent;
    }

    public boolean isServer() {
        return isServer;
    }
    
    
    
     
    
    void connect(){
        mysocket = new Socket();
        try {
            mysocket.connect(new InetSocketAddress(Inet4Address.getByAddress(serveraddress), portnum), 0);            
        } catch (IOException ex) {
            nmfparent.showError();
        }
    }
    
    
    public boolean makeconnection(){
        try {
            if(isServer){
                serversoc = new ServerSocket(portnum);
                serversoc.setSoTimeout(0);
                mysocket = serversoc.accept();
            }else{
                connect();
            }            
            input = new BufferedReader(new InputStreamReader(mysocket.getInputStream()));
            output = new PrintWriter(mysocket.getOutputStream(), true);            
            return true;
        } catch (SocketException ex) { 
            return false;
        }catch (IOException ex) {
            return false;
        }
    }
    
    
    public void sendplayersinformation(String playersinformation){        
        try{
            new Sending(playersinformation).sendplayersinformation();
        }catch(Exception ex){
            nmfparent.showError();
        }
    }
    
    
    public void waitforreceivingdata(){
        try{
            new Receiving().waitforreceivingdata();
        }catch(Exception ex){
            nmfparent.showError();
        }
    }

    
    class Sending implements  Runnable{
        String playersinformation = null;

        public Sending(String playersinformation) {
            this.playersinformation = playersinformation;
        }

        
        public void sendplayersinformation(){
            Thread t = new Thread(this);
            t.start();
        }
        @Override
        public void run() {
            output.println(playersinformation);
            new Receiving().waitforreceivingdata();
        }
        
        
    }
    
    class Receiving implements  Runnable{

        public Receiving() {            
        }

        public void waitforreceivingdata(){
            Thread t = new Thread(this);
            t.start();
        }
        
        @Override
        public void run() {
            while (true) {            
                try {
                    String receivedData = input.readLine();
                    System.out.println(receivedData);
                    if(receivedData.length()>10){
                        nmfparent.ReceivingData(receivedData);
                        break;
                    }else
                        continue;
                } catch (IOException ex) {
                    nmfparent.showError();
                }
            }
        }
        
        
    }
    
        
}


