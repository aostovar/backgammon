package model;

import java.util.ArrayList;
import java.util.List;

public class Player {


	private String name;

	private List<Chip> chips = new ArrayList<Chip>();

	public Player(String name){
		this.name = name;
	}
	
	public List<Chip> getChips() {
		return chips;
	}
	
	public void addChip(Chip chip) {
		chips.add(chip);
		if (chip.getPlayer() != this)
			chip.setPlayer(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

        public int getNumofCaptured(){
            int num=0;
            for(Chip chp : chips)
                if(chp.getState()==Chip.ChipState.Captured)
                    num++;
            
            return num;
        }
        
        //   new method
        
        public int getNumofBeardoff(){
            int num=0;
            for(Chip chp : chips)
                if(chp.getState()==Chip.ChipState.BeardOff)
                    num++;
            
            return num;
        }
        
        //   new method
        public Chip getFirstCaptured(){
            Chip FirstCaptured=null;
            for(Chip chp : chips)
                if(chp.getState()==Chip.ChipState.Captured)
                    FirstCaptured = chp;

            return FirstCaptured;
        }
}
