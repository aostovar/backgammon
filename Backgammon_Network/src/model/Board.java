package model;

import java.awt.Graphics;
import java.awt.Image;

public class Board {
	private Track[] tracks = new Track[24];
        Image image;
        
	public Board(){
            for (int i = 0; i < 24; i++){
                    tracks[i] = new Track(i+1);
            }
	}
	
	public Track[] getTracks() {
		return tracks;
	}
        
        void setImage(Image im){
            image=im;
        }
        
        public Image getImage(){
            return image;
        }                
}
