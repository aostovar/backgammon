package model;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.Random;
import javax.swing.ImageIcon;

public class Dice {	
    
    int number;
    
        
    public void setnumber(int b){
	number=b;
    }
    public int getnumber(){
        return number;
        
    }
	public int rollDice(){
		Random rand = new Random();
		int min = 1;
		int max = 6;

		int randomNum = rand.nextInt(max - min + 1) + min;
                number=randomNum;
		return randomNum;
	}

}
