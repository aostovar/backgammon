package model;

import java.awt.Image;
import java.util.Vector;
import javax.swing.ImageIcon;
import model.Chip.ChipState;

public class Game {
	private Player player1;
	private Player player2;
	private Board board;
	private Dice dice;

	private int dice1num;
	private int dice2num;
        
        private boolean isdice1double = false;
        private boolean isdice2double = false;

	private Player curPlayer;// whose turn is to play
        
        public enum GameStatus {
            switchturn, gotoNextMove, End
        }
        
        GameStatus gStatus;

        
        public void setCurPlayer(Player curPlayer) {
            this.curPlayer = curPlayer;
        }       

        public void setgEvent(GameStatus gEvent) {
            this.gStatus = gEvent;
        }
        
        
        public Player getplayer1(){
            return player1;
        }
        
        
        public Player getplayer2(){
                return player2;
        }
        
        
        public Player getcurPlayer(){
            return curPlayer;
        }        

        public Board getboard(){
                return board;
        }

        public int getdice1num(){
                return dice1num;
        }
        
        public int getdice2num(){
                return dice2num;
        }



	public void init(String playerName1, String playerName2) {
		player1 = new Player(playerName1);
		player2 = new Player(playerName2);

		board = new Board();
		dice = new Dice();

		initBoard();

		curPlayer = player1;
	}

	public void initBoard() {
                ImageIcon imic=new ImageIcon(getClass().getResource("/Images/RedChip.png"));
                Image chip1image=imic.getImage();
                imic=new ImageIcon(getClass().getResource("/Images/BlueChip.png"));
                Image chip2image=imic.getImage();
		// two black chips on 1 track
		board.getTracks()[0].addChip(new Chip(player2, chip2image));
		board.getTracks()[0].addChip(new Chip(player2, chip2image));

		// five white chips on 6 track
		board.getTracks()[5].addChip(new Chip(player1, chip1image));
		board.getTracks()[5].addChip(new Chip(player1, chip1image));
		board.getTracks()[5].addChip(new Chip(player1, chip1image));
		board.getTracks()[5].addChip(new Chip(player1, chip1image));
		board.getTracks()[5].addChip(new Chip(player1, chip1image));

		// tree white chips on 8 track
		board.getTracks()[7].addChip(new Chip(player1, chip1image));
		board.getTracks()[7].addChip(new Chip(player1, chip1image));
		board.getTracks()[7].addChip(new Chip(player1, chip1image));

		// five black chips on 12 track
		board.getTracks()[11].addChip(new Chip(player2, chip2image));
		board.getTracks()[11].addChip(new Chip(player2, chip2image));
		board.getTracks()[11].addChip(new Chip(player2, chip2image));
		board.getTracks()[11].addChip(new Chip(player2, chip2image));
		board.getTracks()[11].addChip(new Chip(player2, chip2image));

		// five white chips on 13 track
		board.getTracks()[12].addChip(new Chip(player1, chip1image));
		board.getTracks()[12].addChip(new Chip(player1, chip1image));
		board.getTracks()[12].addChip(new Chip(player1, chip1image));
		board.getTracks()[12].addChip(new Chip(player1, chip1image));
		board.getTracks()[12].addChip(new Chip(player1, chip1image));

		// tree black chips on 17 track
		board.getTracks()[16].addChip(new Chip(player2, chip2image));
		board.getTracks()[16].addChip(new Chip(player2, chip2image));
		board.getTracks()[16].addChip(new Chip(player2, chip2image));

		// five black chips on 19 track
		board.getTracks()[18].addChip(new Chip(player2, chip2image));
		board.getTracks()[18].addChip(new Chip(player2, chip2image));
		board.getTracks()[18].addChip(new Chip(player2, chip2image));
		board.getTracks()[18].addChip(new Chip(player2, chip2image));
		board.getTracks()[18].addChip(new Chip(player2, chip2image));

		// two white chips on 24 track
		board.getTracks()[23].addChip(new Chip(player1, chip1image));
		board.getTracks()[23].addChip(new Chip(player1, chip1image));
                
                ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Images/Board.jpg"));
                board.setImage(imageIcon.getImage());

                   
	}

	public int[] rollDices() {
		dice1num = dice.rollDice();
		dice2num = dice.rollDice();
                
//                dice1num = 1;
//		dice2num = 1;
                // new
                if(dice1num == dice2num){
                    isdice1double = true;
                    isdice2double = true;
                }
                // up to here

		return new int[] { dice1num, dice2num };
	}

	public int[] getPossibleTracks(Chip chip) {
		int nr = chip.getTrack().getNumber();
		if (curPlayer == player1) {
			return new int[] { isTrackAvailable(nr - dice1num, dice1num),
					isTrackAvailable(nr - dice2num, dice2num) };
		} else {
			return new int[] { isTrackAvailable(nr + dice1num, dice1num),
					isTrackAvailable(nr + dice2num, dice2num) };
		}
	}
        
//        public boolean playerCanMove(){
//            for (Chip chip : curPlayer.getChips())
//                if (getPossibleTracks(chip)[0] != -1 || getPossibleTracks(chip)[1] != -1)
//                    return true;
//            return false;
//        }
        
        public Vector getAvilableTracksForCaptured(Chip chip){
            Vector v=new Vector();
            if (chip.getPlayer() == player1) {
                if(dice1num!=0 && isTrackAvailableForCaptured(board.getTracks()[24 - dice1num].getNumber())){
                    v.add(24 - dice1num + 1);
                }
                if(dice2num!=0 && isTrackAvailableForCaptured(board.getTracks()[24 - dice2num].getNumber())){
                    v.add(24 - dice2num + 1);
                }
                
            }else if (chip.getPlayer() == player2) {
                if(dice1num!=0 && isTrackAvailableForCaptured(board.getTracks()[dice1num-1].getNumber())){
                    v.add(dice1num);
                }
                if(dice2num!=0 && isTrackAvailableForCaptured(board.getTracks()[dice2num-1].getNumber())){
                    v.add(dice2num);
                }
            }
            return v;
        }
        
        public boolean isBearoffable(Chip chip){
            if(chip.getPlayer() == player1){
                for(Chip chps : player1.getChips()){
                    if(chps.getState()== ChipState.Captured || (chps.getState()== ChipState.InGame && chps.getTrack().getNumber() >= 7)){
                        return false;
                    }
                }
                if(chip.getTrack().getNumber() == dice1num || chip.getTrack().getNumber() == dice2num) {
                    return true;
                }else{
                    if(dice1num > chip.getTrack().getNumber() || dice2num > chip.getTrack().getNumber()) {
                        for(Chip chps : player1.getChips()){
                            if(chps.getState()== ChipState.InGame && chps.getTrack().getNumber()  > chip.getTrack().getNumber()){
                                return false;
                            }
                        }
                    }else
                        return false;
                }
            }else{
                for(Chip chps : player2.getChips()){
                    if(chps.getState()== ChipState.Captured || (chps.getState()== ChipState.InGame && chps.getTrack().getNumber() <= 18)){
                        return false;
                    }
                }
                if(chip.getTrack().getNumber() == 24 - dice1num + 1 || chip.getTrack().getNumber() == 24 - dice2num + 1) {
                    return true;
                }else{
                    if(24 - dice1num + 1 < chip.getTrack().getNumber() || 24 - dice2num + 1 < chip.getTrack().getNumber()) {
                        for(Chip chps : player2.getChips()){
                            if(chps.getState()== ChipState.InGame && chps.getTrack().getNumber()  < chip.getTrack().getNumber()){
                                return false;
                            }
                        }
                    }else
                        return false;
                }
            }
            
            return true;
        }

	public GameStatus makeMove(Chip chip, Track track) {
            //new
            if(track == null){
                if(curPlayer == player1){
                    if (dice1num == chip.getTrack().getNumber()) {
                        if(isdice1double)
                            isdice1double = false;
                        else
                            dice1num = 0;
                    } else if (dice2num == chip.getTrack().getNumber()) {
                            if(isdice2double)
                                isdice2double = false;
                            else
                                dice2num = 0;
                    } else if (dice1num > chip.getTrack().getNumber()){
                        if(isdice1double)
                            isdice1double = false;
                        else
                            dice1num = 0;
                    } else if (dice2num > chip.getTrack().getNumber()){
                        if(isdice2double)
                            isdice2double = false;
                        else
                            dice2num = 0;
                    } else{
                        if(isdice1double)
                            isdice1double = false;
                        else
                            dice1num = 0;
                    }
                }else{
                    if (dice1num == 24 - chip.getTrack().getNumber() + 1) {
                            if(isdice1double)
                                isdice1double = false;
                            else
                                dice1num = 0;
                    } else if (dice2num == 24 - chip.getTrack().getNumber() + 1) {
                            if(isdice2double)
                                isdice2double = false;
                            else
                                dice2num = 0;
                    } else if (24 - dice1num + 1 < chip.getTrack().getNumber()){
                        if(isdice1double)
                            isdice1double = false;
                        else
                            dice1num = 0;
                    } else if (24 - dice2num + 1 < chip.getTrack().getNumber()){
                        if(isdice2double)
                            isdice2double = false;
                        else
                            dice2num = 0;
                    } else{
                        if(isdice1double)
                            isdice1double = false;
                        else
                            dice1num = 0;
                    }
                }
                chip.bearOffChip();
            }
            // up to here
            else{
                if(isplayerfree(curPlayer)){
                    int track_dif = Math.abs(chip.getTrack().getNumber()
                                    - track.getNumber());

                    if (dice1num == track_dif) {
                            if(isdice1double)
                                isdice1double = false;
                            else
                                dice1num = 0;
                    } else if (dice2num == track_dif) {
                            if(isdice2double)
                                isdice2double = false;
                            else
                                dice2num = 0;
                    }
                }
                // new 
                else{
                    if(curPlayer == player2){
                        if (dice1num == track.getNumber()) {
                            if(isdice1double)
                                isdice1double = false;
                            else
                                dice1num = 0;
                        } else if (dice2num == track.getNumber()) {
                            if(isdice2double)
                                isdice2double = false;
                            else
                                dice2num = 0;
                        }
                    }else{
                        if (dice1num == 24 - track.getNumber() + 1) {
                            if(isdice1double)
                                isdice1double = false;
                            else
                                dice1num = 0;
                        } else if (dice2num == 24 - track.getNumber() + 1) {
                            if(isdice2double)
                                isdice2double = false;
                            else
                                dice2num = 0;
                        }
                    }
                }
                    //  up to here 

                    if (track.getChips().size() == 1 && track.getChips().get(0).getPlayer() != curPlayer) {
                            track.getChips().get(0).captureChip();
                    }

                    chip.setTrack(track);
            }

            if (!isEndOfGame()) {
                if (dice1num == 0 && dice2num == 0){
                        switchTurn();
                        gStatus=GameStatus.switchturn;
                }else{
                    if(canplayermove(curPlayer)){
                        gStatus=GameStatus.gotoNextMove;
                    }else{
                        switchTurn();
                        gStatus=GameStatus.switchturn;
                    }
                }
            }else
                gStatus=GameStatus.End;

            return gStatus;
                
	}

	public void switchTurn() {
		if (curPlayer == player1)
			curPlayer = player2;
		else
			curPlayer = player1;
                
                // new
                dice1num = 0;
                dice2num = 0;
                isdice1double = false;
                isdice2double = false;
                //up to here
	}

	public boolean isEndOfGame() {
		if(curPlayer.getNumofBeardoff()==15)
                    return true;
                else
                    return false;
	}

	public int isTrackAvailable(int trackNr, int dice) {
            if(trackNr < 1)
                return -1;
            if(trackNr > 24)
                return -1;
            if(dice==0)
                return -1;
            else{
		if (board.getTracks()[trackNr-1].getChips().size() > 1) {
                    if (board.getTracks()[trackNr-1].getChips().get(1).getPlayer() != curPlayer)
                            return -1;
		}
		return trackNr;
            }
	}
        
        public boolean isTrackAvailableForCaptured(int trackNr) {
            if(trackNr <= 6 && trackNr >= 1){
                if(board.getTracks()[trackNr-1].getChips().size()>1 && board.getTracks()[trackNr-1].getChips().get(0).getPlayer()==player1)
                    return false;
                return true;
            }else{
                if(board.getTracks()[trackNr-1].getChips().size()>1 && board.getTracks()[trackNr-1].getChips().get(0).getPlayer()==player2)
                    return false;
                return true;
            }
	}
        
        public boolean isplayerfree(Player player){
            for(Chip chps : player.getChips()){
                if(chps.getState() == ChipState.Captured){
                    return false;
                }
            }
            return true;
        }
        
        public boolean canplayermove(Player curplayer){
            
                if(isplayerfree(curplayer)){
                    for(Chip chps : curplayer.getChips()){
                        if(chps.getState()== ChipState.InGame){
                            if (getPossibleTracks(chps)[0] != -1 || getPossibleTracks(chps)[1] != -1)
                                return true;
                        }
                    }
                    for(Chip chps : curplayer.getChips()){
                        if(chps.getState()== ChipState.InGame){
                            if (isBearoffable(chps))
                                return true;
                        }
                    }
                }else
                    for(Chip chps : curplayer.getChips()){
                        if(chps.getState()== ChipState.Captured){
                            if(getAvilableTracksForCaptured(chps).size()>0){
                                return true;
                            }
                        }
                    }            
            return false;
        }
}
