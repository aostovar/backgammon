package model;

import java.awt.Image;

public class Chip {
	public enum ChipState {
		InGame, Captured, BeardOff
	}
	
	private Player player;
	private ChipState state;
	private Track track;
	Image image=null;        
        
	public Chip(Player player, Image im) {
		setPlayer(player);
		state = ChipState.InGame;
                image = im;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
		if (!player.getChips().contains(this))
			player.addChip(this);
	}
	
	public Player getPlayer(){
		return player;
	}

	public ChipState getState() {
		return state;
	}

	public void setState(ChipState state) {
		this.state = state;
	}

	public Track getTrack() {
		return track;
	}
        
        public void setImage(Image b){
	image=b;
        }
        public Image getImage(){
                return image;
        }

	public void setTrack(Track track) {
                state=ChipState.InGame;
                if(this.track!=null && this.track.getChips().contains(this))
                    this.track.getChips().remove(this);
		this.track = track;
		if (track != null && !track.getChips().contains(this)){
                    track.addChip(this);
		}
	}	
	
	public void captureChip(){
		setTrack(null);
		setState(ChipState.Captured);
	}
	
	public void releaseChip(Track track){
		this.track = track;
		setState(ChipState.InGame);
	}
	
	public void bearOffChip(){
		setTrack(null);
		setState(ChipState.BeardOff);
	}        

}
