package model;

import java.util.ArrayList;
import java.util.List;

public class Track {
	private int number;
	private List<Chip> chips = new ArrayList<Chip>();
	
	public Track(int nr){
		this.number = nr;
	}
	
	public int getNumber(){
		return number;
	}
	
	public List<Chip> getChips() {
		return chips;
	}
	
	public void addChip(Chip chip) {
		chips.add(chip);
		if (chip.getTrack() != this)
                    chip.setTrack(this);
	}
	
	public void removeChip(Chip chip) {
		chips.remove(chip);
		if (chip.getTrack() == this)
                    chip.setTrack(null);
	}        

}
