/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import model.Chip;
import model.Game;
import model.Track;

/**
 *
 * @author ALI
 */
public class MainFrame extends javax.swing.JFrame {

    Game gameEngine = null;
    JPanel jPanel1=null;
    Graphics graphic=null;
    Track selectedtrack=null;
    int[] xpoints=new int[3];
    int[] ypoints=new int[3];
    int possibletracks[]=new int[2];
    Game.GameStatus gamestatus;
    /**
     * Creates new form MainFrame
     */
    public MainFrame(String name1, String name2) {
        gameEngine = new Game();
        gameEngine.init(name1, name2);
//        gameEngine.rollDices();
        System.out.println(gameEngine.getdice1num()+","+gameEngine.getdice2num());
        
        initComponents();
        
        possibletracks[0] = -1;
        possibletracks[1] = -1;
        
        jLabel10.setText(String.valueOf(gameEngine.getplayer1().getNumofBeardoff()));
        jLabel12.setText(String.valueOf(gameEngine.getplayer2().getNumofBeardoff()));
        jLabel4.setText(gameEngine.getcurPlayer().getName());
        jLabel5.setText(gameEngine.getplayer1().getName());
        jLabel6.setText(gameEngine.getplayer2().getName());
        jPanel2.setBackground(Color.red);
        jPanel8.setBackground(Color.red);
        jPanel1 = new JPanel(){
            {
                setOpaque(false);
            }
            
            @Override
            public void paintComponent (Graphics g) {
                graphic=g;

                drawboard(g);
                drawtracks(g);
                drawchips(g);
                drawPossibletracks(g);
                if(gamestatus == Game.GameStatus.End)
                    drawCongratulation(g);
                
                super.paintComponent(g);
            }

            };
        jPanel1.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
        
        jPanel1.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if(!jButton1.isEnabled()){
                    int x=e.getX();
                    int y=e.getY();
                    
                    Track target=getTrackNum(x, y);
                    if(target!=null) {
                        if(target.getChips().size()>0 && target.getChips().get(0).getPlayer()==gameEngine.getcurPlayer()){
                             
                            if(!gameEngine.isplayerfree(gameEngine.getcurPlayer())){
                                if((possibletracks[0]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[0]-1]) || 
                                        (possibletracks[1]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[1]-1])){

                                    gamestatus = gameEngine.makeMove(gameEngine.getcurPlayer().getFirstCaptured(), target);                                        
                                    changeGraphic();
                                    return;

                                }
                            }else if(selectedtrack!=null){
                                if((possibletracks[0]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[0]-1]) || 
                                        (possibletracks[1]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[1]-1])){

                                    boolean IsMove=false;
                                    if(target.getNumber() >=1 && target.getNumber() <=12){
                                        if(y <= getFirstPointofCross(target).y) {
                                            IsMove=true;
                                        }
                                    }else if(target.getNumber() >=13 && target.getNumber() <=24){
                                        if(y >= getFirstPointofCross(target).y) {
                                            IsMove=true;
                                        }
                                    }
                                    if(IsMove){
                                        gamestatus = gameEngine.makeMove(selectedtrack.getChips().get(selectedtrack.getChips().size()-1), target);                                        
                                        changeGraphic();
                                        return;
                                    }else {
                                        selectedtrack=target;
                                        changeselectedtrack();
                                        jPanel1.repaint();
                                        if(gameEngine.isBearoffable(selectedtrack.getChips().get(0)))
                                            jButton3.setEnabled(true);
                                        else
                                            jButton3.setEnabled(false);
                                    }

                                }else {
                                    selectedtrack=target;
                                    changeselectedtrack();
                                    jPanel1.repaint();
                                    if(gameEngine.isBearoffable(selectedtrack.getChips().get(0)))
                                        jButton3.setEnabled(true);
                                    else
                                        jButton3.setEnabled(false);
                                }

                            }else{
                                selectedtrack=target;
                                changeselectedtrack();
                                jPanel1.repaint();
                                if(gameEngine.isBearoffable(selectedtrack.getChips().get(0)))
                                    jButton3.setEnabled(true);
                                else
                                    jButton3.setEnabled(false);
                            }
                        }else if(target.getChips().size()==1 && target.getChips().get(0).getPlayer()!=gameEngine.getcurPlayer()){
                            if(!gameEngine.isplayerfree(gameEngine.getcurPlayer())){
                                if((possibletracks[0]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[0]-1]) || 
                                        (possibletracks[1]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[1]-1])){

                                    gamestatus = gameEngine.makeMove(gameEngine.getcurPlayer().getFirstCaptured(), target);                                        
                                    changeGraphic();
                                    return;
                                }
                            }else if(selectedtrack!=null){
                                if((possibletracks[0]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[0]-1]) || 
                                        (possibletracks[1]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[1]-1])){
                                    gamestatus = gameEngine.makeMove(selectedtrack.getChips().get(selectedtrack.getChips().size()-1), target);                                        
                                    changeGraphic();
                                    return;
                                }
                            }
                        }else{
                            if(!gameEngine.isplayerfree(gameEngine.getcurPlayer())){
                                if((possibletracks[0]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[0]-1]) || 
                                        (possibletracks[1]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[1]-1])){

                                        gamestatus = gameEngine.makeMove(gameEngine.getcurPlayer().getFirstCaptured(), target);                                        
                                        changeGraphic();
                                        return;

                                }
                            }if(selectedtrack!=null){
                                if(possibletracks[0]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[0]-1] ||
                                        possibletracks[1]!=-1 && target==gameEngine.getboard().getTracks()[possibletracks[1]-1]){
                                    gamestatus = gameEngine.makeMove(selectedtrack.getChips().get(selectedtrack.getChips().size()-1), target);                                
                                    changeGraphic();
                                    return;
                                }
                            }
                            
                        }
                    }
                }
                    
                            
                        }

            @Override
            public void mouseReleased(MouseEvent e) {                
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        JPanel jPanel3=new JPanel();
        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(jPanel1);
            jPanel1.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        );

        jPanel1.setBounds(0, 0, 646, 600);

        jPanel2.add(jPanel1);
        jPanel2.validate();

    }
    
    void changeselectedtrack(){
        if(selectedtrack.getNumber()>=19){
            xpoints[0]=(selectedtrack.getNumber()-19)*Size.trackwidth+Size.leftside+Size.middlebar-2;                                
            xpoints[1]=(selectedtrack.getNumber()-19)*Size.trackwidth+Size.leftside+Size.middlebar+Size.trackwidth;
            xpoints[2]=(selectedtrack.getNumber()-19)*Size.trackwidth+Size.leftside+Size.middlebar+Size.trackwidth/2 - 2;

            ypoints[0]=Size.topbar;
            ypoints[1]=Size.topbar;
            ypoints[2]=Size.topbar+Size.tracklength;
//                                selectedtrack.paint(graphic, xpoints, ypoints, Color.RED);
        }else if(selectedtrack.getNumber()>=13){
            xpoints[0]=(selectedtrack.getNumber()-13)*Size.trackwidth-2;
            xpoints[1]=(selectedtrack.getNumber()-13)*Size.trackwidth+Size.trackwidth;
            xpoints[2]=(selectedtrack.getNumber()-13)*Size.trackwidth+Size.trackwidth/2 - 2;

            ypoints[0]=Size.topbar;
            ypoints[1]=Size.topbar;
            ypoints[2]=Size.topbar+Size.tracklength;
        }else if(selectedtrack.getNumber()>=7){
            xpoints[0]=(12-selectedtrack.getNumber())*Size.trackwidth-2;
            xpoints[1]=(12-selectedtrack.getNumber())*Size.trackwidth+Size.trackwidth;
            xpoints[2]=(12-selectedtrack.getNumber())*Size.trackwidth+Size.trackwidth/2 - 2;

            ypoints[0]=Size.boardheight-Size.downbar;
            ypoints[1]=Size.boardheight-Size.downbar;
            ypoints[2]=Size.boardheight-Size.downbar-Size.tracklength;
        }else if(selectedtrack.getNumber()>=1){
            xpoints[0]=(6-selectedtrack.getNumber())*Size.trackwidth+Size.leftside+Size.middlebar-2;
            xpoints[1]=(6-selectedtrack.getNumber())*Size.trackwidth+Size.leftside+Size.middlebar+Size.trackwidth;
            xpoints[2]=(6-selectedtrack.getNumber())*Size.trackwidth+Size.leftside+Size.middlebar+Size.trackwidth/2 - 2;

            ypoints[0]=Size.boardheight-Size.downbar;
            ypoints[1]=Size.boardheight-Size.downbar;
            ypoints[2]=Size.boardheight-Size.downbar-Size.tracklength;
        }

        possibletracks=gameEngine.getPossibleTracks(selectedtrack.getChips().get(selectedtrack.getChips().size()-1));
    }
    
    void changeGraphic(){
        selectedtrack=null;
        possibletracks[0]=-1;
        possibletracks[1]=-1;
        jButton3.setEnabled(false);
        if(gameEngine.getdice1num() == 0){
            jLabel7.setIcon(null);
        }
        if(gameEngine.getdice2num() == 0){
            jLabel8.setIcon(null);
        }
        switch(gamestatus){
            case gotoNextMove:
                if(!gameEngine.isplayerfree(gameEngine.getcurPlayer())){
                    for(Chip chip : gameEngine.getcurPlayer().getChips()) {
                        if(chip.getState()==Chip.ChipState.Captured){
                            Vector v = gameEngine.getAvilableTracksForCaptured(chip);
                            int size=v.size();
                            switch(size){
                                case 2:
                                    possibletracks[0] = (int)v.get(0);
                                    possibletracks[1] = (int)v.get(1);
                                    break;
                                case 1:
                                    possibletracks[0] = (int)v.get(0);
                                    possibletracks[1] = -1;
                                    break;
                                case 0:
                                    possibletracks[0] = -1;
                                    possibletracks[1] = -1;
                                    break;
                            }
                            break;
                        }
                    }
                }
                break;
                
            case switchturn:
                jLabel4.setText(gameEngine.getcurPlayer().getName());
                if(gameEngine.getcurPlayer() == gameEngine.getplayer1()){
                    jLabel4.setForeground(Color.red);
                    jPanel1.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    jPanel2.setBackground(Color.red);
                    jPanel8.setBackground(Color.red);
                }else{
                    jLabel4.setForeground(new Color(98, 132, 0));
                    jPanel1.setBorder(BorderFactory.createLineBorder(new Color(98, 132, 0), 2));
                    jPanel2.setBackground(new Color(98, 132, 0));
                    jPanel8.setBackground(new Color(98, 132, 0));
                }
                jButton1.setEnabled(true);
                jLabel7.setIcon(null);
                jLabel8.setIcon(null);                
                break;
                
            case End:
                jButton1.setEnabled(false);
                jButton3.setEnabled(false);
                selectedtrack = null;                
                break;
        }
        jPanel1.repaint();
    }
    
    void drawCongratulation(Graphics g){
        g.setFont(g.getFont().deriveFont((float)20));
        if(gameEngine.getcurPlayer() == gameEngine.getplayer1()){
            g.setColor(Color.red);
        }else{
            g.setColor(new Color(98, 132, 0));
        }
        g.drawString(gameEngine.getcurPlayer().getName()+" is winner. "+"CONGRATULATIONS.", 
                Size.boardwidth/2-g.getFontMetrics().stringWidth(gameEngine.getcurPlayer().getName()+" is winner. "+"CONGRATULATIONS.")/2, 
                Size.boardwidth/2-g.getFontMetrics().getHeight()/2);
    }
    
    void drawchips(Graphics g){
        //draw chips of player1
        for(int i=0; i<15; i++)
            drawsinglechip(g, gameEngine.getplayer1().getChips().get(i));
        
        //draw chips of player2
        for(int i=0; i<15; i++)
            drawsinglechip(g, gameEngine.getplayer2().getChips().get(i));
    }
    
    void drawtracks(Graphics g){
        if(selectedtrack!=null){
            g.setColor(Color.RED);
            Polygon p=new Polygon(xpoints, ypoints, 3);
            g.fillPolygon(p);
        }
    }
    
    void drawPossibletracks(Graphics g){
        if(possibletracks[0] != -1 || possibletracks[1] != -1){
            g.setColor(Color.RED);
            for(int i=0; i<2; i++){
                int tracknum=possibletracks[i];
                if(tracknum!=-1){
                    Track track=gameEngine.getboard().getTracks()[tracknum-1];
                    Point []pt=getPointsofTrack(track);
//                    if(track.getChips().size()>5)
                    if(tracknum >=1 && tracknum <=12){
                        g.drawLine(pt[0].x+(Size.trackwidth-Size.chipdiameter)/2, pt[0].y-(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter,
                                pt[1].x-(Size.trackwidth-Size.chipdiameter)/2, pt[1].y-(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter-Size.chipdiameter);
                        g.drawLine(pt[0].x+(Size.trackwidth-Size.chipdiameter)/2, pt[0].y-(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter-Size.chipdiameter,
                                pt[1].x-(Size.trackwidth-Size.chipdiameter)/2, pt[1].y-(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter);
                    }else if(tracknum >=13 && tracknum <=24){
                        g.drawLine(pt[0].x+(Size.trackwidth-Size.chipdiameter)/2, pt[0].y+(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter,
                                pt[1].x-(Size.trackwidth-Size.chipdiameter)/2, pt[1].y+(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter+Size.chipdiameter);
                        g.drawLine(pt[0].x+(Size.trackwidth-Size.chipdiameter)/2, pt[0].y+(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter+Size.chipdiameter,
                                pt[1].x-(Size.trackwidth-Size.chipdiameter)/2, pt[1].y+(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter);
                    }
                }
            }
        }
    }
    
    
    Point getFirstPointofCross(Track track){
        int tracknum=track.getNumber();
        Point myPoint=new Point();
        Point []pt=getPointsofTrack(track);
        if(tracknum >=1 && tracknum <=12){
            myPoint.x=pt[0].x+(Size.trackwidth-Size.chipdiameter)/2;
            myPoint.y=pt[0].y-(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter;
        }else if(tracknum >=13 && tracknum <=24){
            myPoint.x=pt[0].x+(Size.trackwidth-Size.chipdiameter)/2;
            myPoint.y=pt[0].y+(track.getChips().size()>5 ? 5 : track.getChips().size())*Size.chipdiameter;
        }
        return myPoint;
    }
    
    
    
    Track getTrackNum(int x, int y){
        Track t=null;
        if(x < Size.boardwidth-Size.rightbar && y > Size.topbar && y < Size.boardheight-Size.downbar){

            if(x > Size.leftside+Size.middlebar){
                // point is on the right side of board
                if(y < Size.topbar+Size.tracklength){
                    //up side

                    int tracknum=((x-Size.leftside-Size.middlebar)/Size.trackwidth)+19;
                    if(tracknum>24)
                        tracknum=24;
                    t=gameEngine.getboard().getTracks()[tracknum-1];

                }else if(y > Size.boardheight-Size.downbar-Size.tracklength){
                    //down side
                    int tracknum=6-((x-Size.leftside-Size.middlebar)/Size.trackwidth);
                    if(tracknum<1)
                        tracknum=1;
                    t=gameEngine.getboard().getTracks()[tracknum-1];
                }

            }else{
                // point is on the left side of board
                if(y < Size.topbar+Size.tracklength){
                    //up side

                    int tracknum=(x/Size.trackwidth)+13;
                    if(tracknum>18)
                        tracknum=18;
                    t=gameEngine.getboard().getTracks()[tracknum-1];
                    
                }else if(y > Size.boardheight-Size.downbar-Size.tracklength){
                    //down side
                    int tracknum=12-(x/Size.trackwidth);
                    if(tracknum<7)
                        tracknum=7;
                    t=gameEngine.getboard().getTracks()[tracknum-1];
                }
            }                    
        }
        return t;
    }

    
    Point[] getPointsofTrack(Track track){
        Point[] pt=new Point[2];
        pt[0]=new Point();
        pt[1]=new Point();
        if(track.getNumber()>=19){
            pt[0].x=(track.getNumber()-19)*Size.trackwidth+Size.leftside+Size.middlebar;                                
            pt[1].x=(track.getNumber()-19)*Size.trackwidth+Size.leftside+Size.middlebar+Size.trackwidth;

            pt[0].y=Size.topbar;
            pt[1].y=Size.topbar;
//                                selectedtrack.paint(graphic, xpoints, ypoints, Color.RED);
        }else if(track.getNumber()>=13){
            pt[0].x=(track.getNumber()-13)*Size.trackwidth;
            pt[1].x=(track.getNumber()-13)*Size.trackwidth+Size.trackwidth;

            pt[0].y=Size.topbar;
            pt[1].y=Size.topbar;
        }else if(track.getNumber()>=7){
            pt[0].x=(12-track.getNumber())*Size.trackwidth;
            pt[1].x=(12-track.getNumber())*Size.trackwidth+Size.trackwidth;

            pt[0].y=Size.boardheight-Size.downbar;
            pt[1].y=Size.boardheight-Size.downbar;
        }else if(track.getNumber()>=1){
            pt[0].x=(6-track.getNumber())*Size.trackwidth+Size.leftside+Size.middlebar;
            pt[1].x=(6-track.getNumber())*Size.trackwidth+Size.leftside+Size.middlebar+Size.trackwidth;

            pt[0].y=Size.boardheight-Size.downbar;
            pt[1].y=Size.boardheight-Size.downbar;
        }
        
        
        
        return pt;
    }
    
    public void drawboard(Graphics g){
        g.drawImage(gameEngine.getboard().getImage(), 0, 0,gameEngine.getboard().getImage().getWidth(null),
                gameEngine.getboard().getImage().getHeight(null),null);
    }
    
    public void drawsinglechip(Graphics g, Chip chip){
        Track track=chip.getTrack();
        if(chip.getState()!=Chip.ChipState.BeardOff){
            g.setColor(Color.RED);
            if(chip.getState() == Chip.ChipState.InGame){
                int tracknumber=track.getNumber();
//                int numofcapturedchips = track.getNumofCaptured();
                if(tracknumber>=19){
                    if(track.getChips().indexOf(chip)>=5){
                        g.drawString(String.valueOf(track.getChips().size()), Size.leftside+Size.middlebar+(tracknumber-19)*Size.trackwidth+((Size.trackwidth-g.getFontMetrics().stringWidth(String.valueOf(track.getChips().size()))) /2), Size.topbar+6*Size.chipdiameter+10);
                    }else
                        g.drawImage(chip.getImage(), Size.leftside+Size.middlebar+(tracknumber-19)*Size.trackwidth+((Size.trackwidth-Size.chipdiameter)/2), Size.topbar+track.getChips().indexOf(chip)*Size.chipdiameter, null);             
                }else if(tracknumber>=13){
                    if(track.getChips().indexOf(chip)>=5){
                        g.drawString(String.valueOf(track.getChips().size()), (tracknumber-13)*Size.trackwidth+((Size.trackwidth-g.getFontMetrics().stringWidth(String.valueOf(track.getChips().size()))) /2), Size.topbar+6*Size.chipdiameter+10);
                    }else
                        g.drawImage(chip.getImage(), (tracknumber-13)*Size.trackwidth+((Size.trackwidth-Size.chipdiameter)/2), Size.topbar+track.getChips().indexOf(chip)*Size.chipdiameter, null);
                }else if(tracknumber>=7){
                    if(track.getChips().indexOf(chip)>=5){
                        g.drawString(String.valueOf(track.getChips().size()), (12-tracknumber)*Size.trackwidth+((Size.trackwidth-g.getFontMetrics().stringWidth(String.valueOf(track.getChips().size()))) /2), Size.boardheight-Size.downbar-6*Size.chipdiameter - 10);
                    }else
                        g.drawImage(chip.getImage(), (12-tracknumber)*Size.trackwidth+((Size.trackwidth-Size.chipdiameter)/2), Size.boardheight-Size.downbar-track.getChips().indexOf(chip)*Size.chipdiameter - Size.chipdiameter, null);
                }else if(tracknumber>=1){
                    if(track.getChips().indexOf(chip)>=5){
                        g.drawString(String.valueOf(track.getChips().size()), Size.boardwidth-Size.rightbar-(tracknumber)*Size.trackwidth+((Size.trackwidth-g.getFontMetrics().stringWidth(String.valueOf(track.getChips().size()))) /2), Size.boardheight-Size.downbar-6*Size.chipdiameter - 10);
                    }else
                        g.drawImage(chip.getImage(), Size.boardwidth-Size.rightbar-(tracknumber)*Size.trackwidth+((Size.trackwidth-Size.chipdiameter)/2), Size.boardheight-Size.downbar-track.getChips().indexOf(chip)*Size.chipdiameter - Size.chipdiameter, null);
                }
            }else if(chip.getState() == Chip.ChipState.Captured){
                g.setColor(Color.WHITE);
                if(chip.getPlayer()==gameEngine.getplayer2()) {
                    {
                        if(chip.getPlayer().getNumofCaptured()>1){
                            g.drawImage(chip.getImage(), Size.leftside+Size.middlebar/2-Size.chipdiameter/2, Size.boardheight/2-50, null);
                            g.drawString(String.valueOf(chip.getPlayer().getNumofCaptured()), 
                                    Size.leftside+Size.middlebar/2-g.getFontMetrics().stringWidth(String.valueOf(chip.getPlayer().getNumofCaptured()))/2, 
                                    Size.boardheight/2-50+Size.chipdiameter/2);
                        }else {
                            g.drawImage(chip.getImage(), Size.leftside+Size.middlebar/2-Size.chipdiameter/2, Size.boardheight/2-50, null);
                        }
                    }
                }
                else{
                    if(chip.getPlayer().getNumofCaptured()>1){
                        g.drawImage(chip.getImage(), Size.leftside+Size.middlebar/2-Size.chipdiameter/2, Size.boardheight/2+30, null);
                        g.drawString(String.valueOf(chip.getPlayer().getNumofCaptured()), 
                                Size.leftside+Size.middlebar/2-g.getFontMetrics().stringWidth(String.valueOf(chip.getPlayer().getNumofCaptured()))/2, 
                                Size.boardheight/2+30+Size.chipdiameter/2);
                    }else {
                        g.drawImage(chip.getImage(), Size.leftside+Size.middlebar/2-Size.chipdiameter/2, Size.boardheight/2+30, null);
                    }
                }
                    }
        }
        }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dices", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, null, new java.awt.Color(0, 0, 255)));

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/dice.png"))); // NOI18N
        jButton1.setText("Roll Dices");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(101, 101, 101))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jButton2.setText("New Game");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jButton3.setText("Bear Off");
        jButton3.setEnabled(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Player Status", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, null, new java.awt.Color(0, 0, 255)));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 255));
        jLabel1.setText("Current Player:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 0, 0));

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Player 1", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, null, new java.awt.Color(0, 0, 255)));

        jLabel5.setForeground(new java.awt.Color(255, 0, 0));

        jLabel2.setText("Name:");

        jLabel9.setText("Beard-off Chips:");

        jLabel10.setForeground(new java.awt.Color(255, 0, 0));
        jLabel10.setText("0");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(7, 7, 7))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Player 2", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, null, new java.awt.Color(0, 0, 255)));

        jLabel3.setText("Name:");

        jLabel6.setForeground(new java.awt.Color(98, 132, 0));

        jLabel11.setText("Beard-off Chips:");

        jLabel12.setForeground(new java.awt.Color(98, 132, 0));
        jLabel12.setText("0");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 7, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 646, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        gameEngine.rollDices();
        jLabel7.setIcon(new ImageIcon(getClass().getResource("/Images/dice_"+gameEngine.getdice1num()+".png")));
        jLabel8.setIcon(new ImageIcon(getClass().getResource("/Images/dice_"+gameEngine.getdice2num()+".png")));
        
        possibletracks[0] = -1;
        possibletracks[1] = -1;
        selectedtrack = null;
        jButton3.setEnabled(false);
        
        if(gameEngine.canplayermove(gameEngine.getcurPlayer())){
            jButton1.setEnabled(false);
            if(!gameEngine.isplayerfree(gameEngine.getcurPlayer())){
                for(Chip chip : gameEngine.getcurPlayer().getChips()) {
                    if(chip.getState()==Chip.ChipState.Captured){
                        Vector v = gameEngine.getAvilableTracksForCaptured(chip);
                        int size=v.size();
                        switch(size){
                            case 2:
                                possibletracks[0] = (int)v.get(0);
                                possibletracks[1] = (int)v.get(1);
                                break;
                            case 1:
                                possibletracks[0] = (int)v.get(0);
                                possibletracks[1] = -1;
                                break;
                            case 0:
                                possibletracks[0] = -1;
                                possibletracks[1] = -1;
                                break;
                        }
                        break;
                    }
                }
                repaint();
                        
            }
        }else{
            JOptionPane.showConfirmDialog(this, "You can not move any of your chips.", "BackGammon", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE);
            jLabel7.setIcon(null);
            jLabel8.setIcon(null);
            jButton1.setEnabled(true);
            gameEngine.switchTurn();
            jLabel4.setText(gameEngine.getcurPlayer().getName());
            if(gameEngine.getcurPlayer() == gameEngine.getplayer1()){
                jLabel4.setForeground(Color.red);
                jPanel1.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                jPanel2.setBackground(Color.red);
                jPanel8.setBackground(Color.red);
            }else{
                jLabel4.setForeground(new Color(98, 132, 0));
                jPanel1.setBorder(BorderFactory.createLineBorder(new Color(98, 132, 0), 2));
                jPanel2.setBackground(new Color(98, 132, 0));
                jPanel8.setBackground(new Color(98, 132, 0));
            }
        }
            
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
            if(gameEngine.isBearoffable(selectedtrack.getChips().get(selectedtrack.getChips().size()-1))){
                gamestatus = gameEngine.makeMove(selectedtrack.getChips().get(selectedtrack.getChips().size()-1), null);
                jLabel10.setText(String.valueOf(gameEngine.getplayer1().getNumofBeardoff()));
                jLabel12.setText(String.valueOf(gameEngine.getplayer2().getNumofBeardoff()));
                changeGraphic();
            }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int i=JOptionPane.showConfirmDialog(this, "Do you want to start a new game?", "BackGammon", JOptionPane.YES_NO_OPTION);
        if(i==0){
            String name1=gameEngine.getplayer1().getName();
            String name2=gameEngine.getplayer2().getName();
            gameEngine = new Game();
            gameEngine.init(name1, name2);
    //        gameEngine.rollDices();
            selectedtrack = null;
            possibletracks[0]=-1;
            possibletracks[1]=-1;
            jButton3.setEnabled(false);
            jButton1.setEnabled(true);
            jLabel7.setIcon(null);
            jLabel8.setIcon(null);
            jLabel4.setText(gameEngine.getplayer1().getName());
            jLabel4.setForeground(Color.red);
            jPanel2.setBackground(Color.red);
            jPanel8.setBackground(Color.red);
            jLabel10.setText("0");
            jLabel12.setText("0");
            gamestatus = Game.GameStatus.switchturn;
            jPanel1.repaint();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new MainFrame().setVisible(true);
//            }
//        });
//    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    // End of variables declaration//GEN-END:variables
}
